<?php
/**
 * @file
 * menu-tree.func.php
 */

/**
 * Overrides theme_menu_tree().
 */
function palladium_bootstrap_menu_tree(&$variables) {
  return '<ul class="menu menu-usgs nav ">' . $variables['tree'] . '</ul>';
}

/**
 * Bootstrap theme wrapper function for the primary menu links.
 */
function palladium_bootstrap_menu_tree__primary(&$variables) {
  return '<ul class="menu menu-usgs nav navbar-nav ">' . $variables['tree'] . '</ul>';
}

/**
 * Bootstrap theme wrapper function for the secondary menu links.
 */
function palladium_bootstrap_menu_tree__secondary(&$variables) {
  return '<ul class="menu menu-usgs nav navbar-nav secondary">' . $variables['tree'] . '</ul>';
}
